from rest_framework import serializers
from .models import Casos, Procedimiento, Dependencia, CasoDependencia, Persona, Estado, Usuario, UsuarioDependencia, Rol, Distribucion, Historico, DependenciaDist, Language

class ProcedimientoSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Procedimiento
        fields = "__all__"  

class EstadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estado
        fields = "__all__"

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = "__all__"

class RolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rol
        fields = "__all__"

class UsuarioDependenciaSerializer(serializers.ModelSerializer):
    usuario = UsuarioSerializer(many=False, read_only=True)
    rol = RolSerializer(many=False, read_only=True)
    class Meta:
        model = UsuarioDependencia
        fields = "__all__"

class DependenciaSerializer(serializers.ModelSerializer):
    estado = EstadoSerializer(many=False, read_only=True)
    
    class Meta:
        model = Dependencia
        fields = "__all__"

class DependenciaSoloSerializer(DependenciaSerializer):   
    usuario_dependencias = UsuarioDependenciaSerializer(many=True, read_only=True)

class CasoDependenciaSerializer(serializers.ModelSerializer):
    dependencia = DependenciaSerializer(many=False)    
    class Meta:
        model = CasoDependencia
        fields = "__all__"  

class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Persona
        fields = "__all__"        

class CasoSerializer(serializers.ModelSerializer):
    procedimiento = ProcedimientoSerializer(many=False, read_only=True) 
    procedimiento_id = serializers.IntegerField()  
    caso_dependencias = CasoDependenciaSerializer(many=True, read_only=True)
    personas = PersonaSerializer(many=True, read_only=True)
    
    class Meta:
        model = Casos
        fields = "__all__" 

class DependenciaDistSerializer(serializers.ModelSerializer):
    class Meta:
        model = DependenciaDist
        fields = "__all__"        

class HistoricoSerializer(serializers.ModelSerializer):
    dependencias = DependenciaDistSerializer(many=False, read_only=True)
    class Meta:
        model = Historico
        fields = "__all__"

class DistribucionSerializer(serializers.ModelSerializer):
    historicos = HistoricoSerializer(many=True, read_only=True)
    dependencias = DependenciaDistSerializer(many=False, read_only=True)
    class Meta:
        model = Distribucion
        fields = "__all__"

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = "__all__"