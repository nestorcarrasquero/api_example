from django.conf.urls import url
from .views import CasoView, ProcedimientoView, DependenciaView, CasoDependenciaView, CasoDetailView, EstadoView, DistribucionView, LanguageView

urlpatterns = [   
    url(r'^casos/$', CasoView.as_view()),
    url(r'^casos/(?P<id>\d+)/$', CasoDetailView.as_view()),
    url(r'^procedimientos/$', ProcedimientoView.as_view()),
    url(r'^dependencias/$', DependenciaView.as_view()),
    url(r'^caso_dependencias/$', CasoDependenciaView.as_view()),
    url(r'^estados/$', EstadoView.as_view()),
    url(r'^distribucion/$', DistribucionView.as_view()),   
    url(r'^languages/$', LanguageView.as_view()),   
]