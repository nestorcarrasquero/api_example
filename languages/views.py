from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.response import Response
from .models import Casos, Procedimiento, Dependencia, CasoDependencia, Estado, Distribucion, Language
from .serializers import CasoSerializer, ProcedimientoSerializer, DependenciaSerializer, CasoDependenciaSerializer, EstadoSerializer, DependenciaSoloSerializer, DistribucionSerializer, LanguageSerializer
from django.db.models import Q

# Create your views here.
class ProcedimientoView(generics.ListAPIView):
    queryset = Procedimiento.objects.all()
    serializer_class = ProcedimientoSerializer
    filter_backends = [SearchFilter]
    search_fields = ['descripcion']

class DependenciaView(generics.ListAPIView):    
    serializer_class = DependenciaSoloSerializer

    def get_queryset(self):
        descripcion = self.request.query_params.get('descripcion', None)
        estado = self.request.query_params.get('estado', None)
        adscripcion = self.request.query_params.get('adscripcion', None)
        qs = None

        if descripcion is not None:
            qs = Dependencia.objects.filter(Q(descripcion__icontains=descripcion))

        if estado is not None:
            if qs is not None:
                qs = qs & Dependencia.objects.filter(estado__id=estado)
            else:
                qs = Dependencia.objects.filter(estado__id=estado)

        if adscripcion is not None:
            if qs is not None:
                qs = qs & Dependencia.objects.filter(Q(adscripcion_descripcion__icontains=adscripcion))
            else:
                qs = Dependencia.objects.filter(Q(adscripcion_descripcion__icontains=adscripcion))
        return qs

class CasoFilter(filters.FilterSet):
    identificador_caso = filters.CharFilter(field_name='identificador_caso', lookup_expr='icontains')

    class Meta:
        model = Casos
        fields = ['identificador_caso']

class CasoView(generics.ListCreateAPIView):    
    serializer_class = CasoSerializer

    def get_queryset(self):        
        identificador = self.request.query_params.get('identificador', None)  
        referencia = self.request.query_params.get('referencia', None)  
        persona = self.request.query_params.get('cedula', None)  
        qs = None  

        if identificador is not None:            
            qs = Casos.objects.filter(Q(identificador_caso__icontains=identificador))
        
        if referencia is not None:            
            if qs is not None:
                qs = qs & Casos.objects.filter(Q(referencia__icontains=referencia)) 
            else:
                qs = Casos.objects.filter(Q(referencia__icontains=referencia)) 

        if persona is not None:
            if qs is not None:
                qs = qs & Casos.objects.filter(persona__cedula=persona)
            else:
                qs = Casos.objects.filter(persona__cedula=persona)
        return qs

class CasoDetailView(generics.RetrieveUpdateAPIView):
    queryset = Casos.objects.all()
    serializer_class = CasoSerializer
    lookup_field = 'id'

class CasoDependenciaView(generics.ListAPIView):
    queryset = CasoDependencia.objects.all()[:50]
    serializer_class = CasoDependenciaSerializer  

class EstadoView(generics.ListAPIView):
    queryset = Estado.objects.all()
    serializer_class = EstadoSerializer      

class DistribucionView(generics.ListAPIView):
    serializer_class = DistribucionSerializer
    
    def get_queryset(self):
        identificador = self.request.query_params.get('identificador', None)
        dependencia = self.request.query_params.get('dependencia', None)
        qs = None
        if identificador is not None:
            qs = Distribucion.objects.using('distribucion').filter(Q(numero_unico__icontains=identificador))
        if dependencia is not None:
            if qs is not None:
                qs = qs & Distribucion.objects.using('distribucion').filter(dependencias_id=dependencia)
            else:
                qs = Distribucion.objects.using('distribucion').filter(dependencias_id=dependencia) 
        return qs

class LanguageView(generics.ListAPIView):
    serializer_class = LanguageSerializer
    queryset = Language.objects.all()