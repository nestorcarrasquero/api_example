from django.db import models

class DependenciaDist(models.Model):
    descripcion = models.CharField(max_length=100)
    estado_descripcion = models.CharField(max_length=50)
    adscripcion_descripcion = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'dependencias'

class Distribucion(models.Model):
    anio = models.IntegerField(null=True)
    correlativo = models.IntegerField(null=True)
    numero_unico = models.CharField(max_length=20)
    fecha_registro = models.DateField(null=True)
    fecha_hecho = models.DateField(null=True)
    excluido = models.BooleanField()
    resulta = models.BooleanField()
    comision = models.BooleanField()
    fdistribucion = models.DateField(null=True)
    dependencias = models.ForeignKey(
        DependenciaDist,
        on_delete=models.CASCADE,
        related_name="dependencias_1",
        related_query_name="dependencia_1",
        db_column="dependencias_id"
    )

    class Meta:
        managed = False
        db_table = 'distribucion'        

    def __str__(self):
        return str(self.numero_unico)

class Historico(models.Model):
    fdistribucion = models.DateField()
    ssc = models.BooleanField()
    activo_ssc = models.BooleanField()
    fecha_fisico = models.DateTimeField()
    motivo_aceptar_rechazar = models.BooleanField()
    pendiente_aceptar = models.BooleanField()
    distribucion = models.ForeignKey(
        Distribucion,
        on_delete=models.CASCADE,
        related_name="historicos",
        related_query_name="historico"
    )
    dependencias = models.ForeignKey(
        DependenciaDist,
        on_delete=models.CASCADE,
        related_name="dependencias",
        related_query_name="dependencia"
    )    

    class Meta:
        managed = False
        db_table = 'historicodistribucion'