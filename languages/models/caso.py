from django.db import models

# Create your models here.
class Language(models.Model):
    name = models.CharField(max_length=50)
    paradigm = models.CharField(max_length=50)

    class Meta:
        managed = False       
        db_table = 'languages'

class Procedimiento(models.Model):
    descripcion = models.CharField(max_length=100)   

    class Meta:
        managed = False
        db_table = 'procedimientos'

class Estado(models.Model):
    descripcion = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'estados'

class Dependencia(models.Model):
    descripcion = models.CharField(max_length=100)
    estado_descripcion = models.CharField(max_length=50)
    adscripcion_descripcion = models.CharField(max_length=100)
    #adscripcion_superior_descripcion = models.CharField(max_length=100)

    estado = models.ForeignKey(
        Estado,
        on_delete=models.CASCADE,
        related_name="estados",
        related_query_name="estado"
    )

    class Meta:
        managed = False
        db_table = 'dependencias'

class Casos(models.Model):
    identificador_caso = models.CharField(max_length=20, null=False)
    referencia = models.CharField(max_length=30, null=False)   
    descripcion_hecho = models.CharField(max_length=100, null=False)
    fecha_hecho = models.DateField(null=True) 
    fecha_inicio = models.DateField(null=True) 
    fecha_recepcion = models.DateField(null=True) 
    fecha_registro = models.DateTimeField(auto_now_add=True)    

    procedimiento = models.ForeignKey(
        Procedimiento, 
        on_delete=models.CASCADE, 
        related_name="casos", 
        related_query_name="caso"
        )
    
    class Meta:
        managed = False
        db_table = 'casos'

    def __str__(self):
        return str(self.identificador_caso)
       

class CasoDependencia(models.Model):
    estatus = models.CharField(max_length=15)
    rol = models.CharField(max_length=20)
    fecha_activacion = models.DateField()
    fecha_desactivacion = models.DateField()
    caso = models.ForeignKey(
        Casos,
        on_delete=models.CASCADE,
        related_name="caso_dependencias",
        related_query_name="caso_dependencia"
    )
    dependencia = models.ForeignKey(
        Dependencia,
        on_delete=models.CASCADE,
        related_name="caso_dependencias",
        related_query_name="caso_dependencia"
    )

    class Meta:
        managed = False
        db_table = 'caso_dependencias'
        ordering = ["caso_id"]

class Persona(models.Model):
    cedula = models.CharField(max_length=10)
    nombres = models.CharField(max_length=30)        
    apellidos = models.CharField(max_length=30)
    caso = models.ForeignKey(
        Casos,
        on_delete=models.CASCADE,
        related_name="personas",
        related_query_name="persona"
    )

    class Meta:
        managed = False
        db_table = 'personas'

class Usuario(models.Model):
    cedula = models.CharField(max_length=10)
    nombres = models.CharField(max_length=30)
    apellidos = models.CharField(max_length=30)
    suspendido = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'usuarios'

class Rol(models.Model):
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'roles'   

class UsuarioDependencia(models.Model):
    estatus_activacion = models.BooleanField()
    fecha_activacion = models.DateTimeField()
    fecha_desactivacion = models.DateTimeField()
    usuario = models.ForeignKey(
        Usuario,
        on_delete=models.CASCADE,
        related_name="usuario_dependencias",
        related_query_name="usuario_dependencia"
    )
    dependencia = models.ForeignKey(
        Dependencia,
        on_delete=models.CASCADE,
        related_name="usuario_dependencias",
        related_query_name="usuario_dependencia"
    )
    rol = models.ForeignKey(
        Rol,
        on_delete=models.CASCADE,
        related_name="usuario_dependencias",
        related_query_name="usuario_dependencia"
    )    
    class Meta:
        managed = False
        db_table = 'usuario_dependencias'