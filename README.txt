PROYECTO api_example:
- Proyecto personal con el objeto de demostrar avances en el uso de django rest framework
- Consta de dos archivos: caso.py y distribucion.py (modelos), validaciones y relaciones entre modelos
- El archivo serializers.py contiene clases con atributos vinculados entre modelos bajo la relacion 
1:N (caso->caso_dependencias; caso->personas; distribucion->historicos) y 1:1 (caso->procedimiento)
- Autenticación vía TOKEN (django-rest-auth)
- Conexión a postgresql vía psycopg2

Algunos de los principales enlaces: 
- /rest-auth/login/ -> ingreso de usuario. Salida: TOKEN
- /rest-auth/logout/ -> salida de usuario. Entrada: TOKEN
- /rest-auth/registration/ -> registro de usuario. Entrada: usuario, contraseña y correo
- /api/languages/ -> lista de idiomas (modelo)